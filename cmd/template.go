/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"encoding/csv"
	"fmt"
	"io"
	"io/ioutil"
	"os"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/RenonT1805/csv-processor/template"
)

type TemplateOptions struct {
	CSV          string `validate:"required"`
	Template     string `validate:"required"`
	Out          string
	SeparateChar string `validate:"len=1"`
	Format       string
}

var (
	templateOptions = &TemplateOptions{}
)

// templateCmd represents the template command
var templateCmd = &cobra.Command{
	Use:   "template",
	Short: "Convert csv to json",
	Long:  ``,
	PreRunE: func(cmd *cobra.Command, args []string) error {
		return validateParams(*templateOptions)
	},
	Run: runTemplate,
}

func init() {
	rootCmd.AddCommand(templateCmd)
	templateCmd.Flags().StringVarP(&templateOptions.Out, "out", "o", "", "変換結果の出力ファイルのパスを指定します")
	templateCmd.Flags().StringVarP(&templateOptions.CSV, "csv", "c", "", "変換元のCSVファイルのパスを指定します(必須)")
	templateCmd.Flags().StringVarP(&templateOptions.Template, "template", "t", "", "変換元のテンプレートファイルを指定します(必須)")
	templateCmd.Flags().StringVarP(&templateOptions.SeparateChar, "separate", "s", ",", "CSVの分割文字を設定します")
	templateCmd.Flags().StringVarP(&templateOptions.Format, "format", "f", "", "テンプレートファイルのフォーマットを指定します")
}

func runTemplate(cmd *cobra.Command, args []string) {
	csvFile, err := os.Open(templateOptions.CSV)
	if err != nil {
		logrus.Errorln(err)
		os.Exit(1)
	}
	defer csvFile.Close()

	r := csv.NewReader(csvFile)
	r.Comma = rune(templateOptions.SeparateChar[0])

	csvMap := map[string][]string{}
	headers, err := r.Read()
	var dataCount int
	if err != nil {
		logrus.Infoln("CSVファイルの読み込みに失敗しました")
		os.Exit(1)
	}
	for _, header := range headers {
		csvMap[header] = []string{}
	}

	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			logrus.Infoln("CSVデータの読み込みに失敗しました")
			os.Exit(1)
		}

		for idx, item := range record {
			csvMap[headers[idx]] = append(csvMap[headers[idx]], item)
		}
		dataCount++
	}

	templateFile, err := os.Open(templateOptions.Template)
	if err != nil {
		logrus.Errorln(err)
		os.Exit(1)
	}
	defer templateFile.Close()

	templateSource, err := ioutil.ReadAll(templateFile)
	if err != nil {
		logrus.Errorln(err)
		os.Exit(1)
	}

	result, err := template.GenerateFromTemplate(csvMap, dataCount, string(templateSource), template.StringToTemplateFormat(templateOptions.Format))
	if err != nil {
		logrus.Errorln(err)
		os.Exit(1)
	}

	if templateOptions.Out == "" {
		_, err = fmt.Fprintf(logrus.StandardLogger().Out, "\n%s\n", result)
	} else {
		err = os.WriteFile(templateOptions.Out, []byte(result), 0666)
	}
	if err != nil {
		logrus.Errorln(err)
		os.Exit(1)
	}
}
