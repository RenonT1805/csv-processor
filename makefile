run-simpletemplate:
	go build
	csv-processor.exe template -o=sample/simpletemplate_result.json -c=sample/sample.csv -t=sample/simpletemplate_sample.tmpl

run-gotemplate:
	go build
	csv-processor.exe template -o=sample/gotemplate_result.json -c=sample/sample.csv -t=sample/gotemplate_sample.tmpl -f=GO