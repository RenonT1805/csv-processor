package template

import (
	"errors"
)

func GenerateFromTemplate(csvData map[string][]string, dataCount int, templateSource string, templateFormat TemplateFormat) (string, error) {
	switch templateFormat {
	case SimpleTemplate:
		return generateFromSimpleTemplate(csvData, dataCount, templateSource), nil
	case GoTemplate:
		return generateFromGoTemplate(csvData, dataCount, templateSource)
	}
	return "", errors.New("不正なテンプレートフォーマット")
}
